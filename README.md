
# requirements

 - vagrant
 - ansible

# setup

```
$ git clone https://gitlab.com/wybczu/apache2-git-setup-demo.git
$ cd apache2-git-setup-demo
$ vagrant up
```

# running tests

```
$ vagrant ssh
$ /vagrant/tests/git-permissions.bats
```

