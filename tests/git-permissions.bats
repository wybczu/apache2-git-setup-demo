#!/usr/bin/env bats

# helpers

TMP_CHECKOUT="$( mktemp -d )"

setup() {
    mkdir -p "${TMP_CHECKOUT}"
}

teardown() {
    rm -rf "${TMP_CHECKOUT}"
}

# we assume that password == username
git_url() {
    local user=$1
    local repo=$2

    echo "http://${user}:${user}@localhost/${repo}.git"
}

# repo1

@test "repo1 => user1 should not be able to push" {
    run git clone "$( git_url user1 repo1 )" "${TMP_CHECKOUT}"
    [ $status -eq 0 ]

    cd "${TMP_CHECKOUT}"
    date >> file.txt
    git add . && git commit -m 'test'
    run git push origin master
    [ $status -eq 1 ]
    [[ ${lines[0]} == "error: RPC failed; result=22, HTTP code = 401" ]]
}

@test "repo1 => user2 should be able to push" {
    run git clone "$( git_url user2 repo1 )" "${TMP_CHECKOUT}"
    [ $status -eq 0 ]

    cd "${TMP_CHECKOUT}"
    date >> file.txt
    git add . && git commit -m 'test'
    run git push origin master
    [ $status -eq 0 ]
}

@test "repo1 => user3 should not have access" {
    run git clone "$( git_url user3 repo1 )" "${TMP_CHECKOUT}"
    [ $status -eq 128 ]
    [[ ${lines[0]} == "Cloning into"* ]]
    [[ ${lines[1]} == "fatal: Authentication failed"* ]]
}

# repo2

@test "repo2 => user1 should be able to push" {
    run git clone "$( git_url user1 repo2 )" "${TMP_CHECKOUT}"
    [ $status -eq 0 ]

    cd "${TMP_CHECKOUT}"
    date >> file.txt
    git add . && git commit -m 'test'
    run git push origin master
    [ $status -eq 0 ]
}

@test "repo2 => user2 should be able to push" {
    run git clone "$( git_url user2 repo2 )" "${TMP_CHECKOUT}"
    [ $status -eq 0 ]

    cd "${TMP_CHECKOUT}"
    date >> file.txt
    git add . && git commit -m 'test'
    run git push origin master
    [ $status -eq 0 ]
}

@test "repo2 => user3 should not have access" {
    run git clone "$( git_url user3 repo2 )" "${TMP_CHECKOUT}"
    [ $status -eq 128 ]
    [[ ${lines[0]} == "Cloning into"* ]]
    [[ ${lines[1]} == "fatal: Authentication failed"* ]]
}

# repo3

@test "repo3 => user1 should not have access" {
    run git clone "$( git_url user1 repo3 )" "${TMP_CHECKOUT}"
    [ $status -eq 128 ]
    [[ ${lines[0]} == "Cloning into"* ]]
    [[ ${lines[1]} == "fatal: Authentication failed"* ]]
}

@test "repo3 => user2 should not have access" {
    run git clone "$( git_url user2 repo3 )" "${TMP_CHECKOUT}"
    [ $status -eq 128 ]
    [[ ${lines[0]} == "Cloning into"* ]]
    [[ ${lines[1]} == "fatal: Authentication failed"* ]]
}

@test "repo3 => user3 should be able to push" {
    run git clone "$( git_url user3 repo3 )" "${TMP_CHECKOUT}"
    [ $status -eq 0 ]

    cd "${TMP_CHECKOUT}"
    date >> file.txt
    git add . && git commit -m 'test'
    run git push origin master
    [ $status -eq 0 ]
}
